#include<iostream>
#include<Eigen/Core>
#include<Eigen/Geometry>
using namespace std;

// 1. Quaterniond(w,z,y,z)
// 2. should be normalized before passing to Matrix3d
// 3. q=q.normalized() is not allowed


Eigen::Quaterniond q(0.1,0.35,0.2,0.3);
Eigen::Quaterniond qn=q.normalized();
//R, R^T, R^-1, RR^T

int main(){
	Eigen::Matrix3d R(qn);
	cout<<R<<endl<<R.transpose()<<endl<<R.inverse()<<endl<<R*R.transpose()<<endl;
	return 0;
}
